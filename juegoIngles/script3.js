

/* En resumen, este código crea un juego donde se muestra una palabra aleatoria en la pantalla y el usuario debe ingresar esa palabra lo más rápido posible antes de que se acabe el tiempo. Cada palabra ingresada correctamente aumenta el puntaje y el usuario tiene la opción de reiniciar el juego al final*/
const words = {
  'californication': 14,
  'plataforma5': 12,
  'black': 5,
  'summer': 6,
  'flea': 4,
  'aeroplane': 9,
  'peppers': 7,
  'unlimited': 9,
  'arcadium': 8,
  'love': 4,
  'getaway': 7,
  'stadium': 7,
  'quixoticelixer': 14,
  'quarter': 7,
  'snow': 4,
  'dylan': 5,
  'zephyr': 6,
  'funky': 5,
  'chili': 5,
  'book': 4,
  'computer': 8,
  'water': 5,
  'tree': 4,
  'dog': 3,
  'music': 5,
  'sun': 3,
  'cloud': 5,
  'ocean': 5,
  'moon': 4,
  'phone': 5,
  'table': 5,
  'chair': 5,
  'desk': 4,
  'car': 3
}

//toma un objeto y devuelve una clave aleatoria
function getRandomWord(obj) {
  const keys = Object.keys(obj); // obtiene las claves del objeto
  const randomKey = keys[Math.floor(Math.random() * keys.length)]; //elige una clave aleatoria
  return randomKey; // devuelve la clave seleccionada (que es una palabra)
}

let palabraAleatoria  = getRandomWord(words);//La clave seleccionada se asigna a la variable
let time = 10;
let score = 0;


//utiliza para actualizar el contenido de un elemento en el DOM usando 
function addToDOM() {
  palabraAleatoria = getRandomWord(words); // obtiene una nueva palabra aleatoria
  document.querySelector("#randomWord").innerText = palabraAleatoria; // actualiza el contenido del elemento
}

addToDOM()

//actualiza el puntaje cada vez que se ingresa correctamente la palabra aleatoria
function updateScore() {
  score++;
  document.querySelector("#score").innerHTML = score;
  }
  
//detecta las entradas del usuario.Cada vez que el usuario ingresa una palabra, la función  verifica si coincide con la palabra aleatoria actual. Si coinciden, se actualiza la palabra aleatoria, se incrementa el puntaje(llamando la funcion updateScore), se vacia el input
document.getElementById("text").addEventListener("input", function(e) {
let palabraIngresada = e.target.value;
if (palabraIngresada === palabraAleatoria) {
  time += 3; //suma tiempo
  e.target.value = ""; //vacia el input
  
  addToDOM(); //renueva la palabra del h1
  updateScore(); //suma puntos
}
});

 //crea un h1 de fin de juego, un parrafo con el puntaje y un bonton que si lo cliqueas vueve a recargar la pagina para volver a empezar
 function gameOver(score) {
  let endGameContainer = document.getElementById("end-game-container");

  let title = document.createElement("h1");
  title.innerText = "Se terminó el tiempo!";
  endGameContainer.appendChild(title);

  let scoreText = document.createElement("p");
  scoreText.innerText = "Puntaje final: " + score;
  endGameContainer.appendChild(scoreText);

  let restartButton = document.createElement("button");
  restartButton.innerText = "Volver a empezar";
  restartButton.onclick = function() { location.reload(); };
  endGameContainer.appendChild(restartButton);
}
// establece un intervalo de tiempo que ejecuta la función actualizarTiempo cada 1000 milisegundos (1 segundo). El valor devuelto por setInterval() es un identificador numérico del intervalo, que se almacena en la variable timeInterval en este caso. Esta variable puede ser utilizada para detener el intervalo en cualquier momento usando clearInterval(timeInterval).se utiliza para actualizar el tiempo restante cada segundo
let timeInterval = setInterval(actualizarTiempo, 1000);

// se ejecuta cada segundo para actualizar el tiempo restante y detener el juego una vez que el tiempo llega a cero
function actualizarTiempo() {
  time--;
  document.querySelector("#timeSpan").innerHTML = time + "s";
  if (time === 0) {
      clearInterval(timeInterval);
    }
  if (time === 0) {
    clearInterval(timeInterval);
    gameOver(score); 
    }
 }



